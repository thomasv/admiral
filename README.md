# WIP

Admiral aims to make it easy to manage and inspect multiple Helm tillers
by providing an easy to use interface and JSON API to interact with said tillers.

## Development

```
# build go binary and a container
./bin/admiral-build

# Run UI
cd ui && yarn install && yarn serve
```
