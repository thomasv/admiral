import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  state: {
    tillers: [
      {
        id: "foo",
        name: "Foo",
        cluster_id: "kubernetes.local",
        releases: [
          {
            name: "elasticsearch",
            revision: 2,
            updated: "Thu Jun 28 09:19:52 2018",
            status: "deployed",
            chart: "wa-elasticsearch-0.1.19",
            namespace: "foo-staging-monitoring"
          },
          {
            name: "mongodb",
            revision: 15,
            updated: "Thu Jun 28 09:19:52 2018",
            status: "deployed",
            chart: "wa-mongodb-0.1.19",
            namespace: "foo-staging-db"
          },
          {
            name: "redis",
            revision: 8,
            updated: "Thu Jun 28 09:19:52 2018",
            status: "deployed",
            chart: "wa-redis-0.1.19",
            namespace: "foo-staging-redis"
          },
          {
            name: "postgres",
            revision: 4,
            updated: "Thu Jun 28 09:19:52 2018",
            status: "deployed",
            chart: "wa-postgres-0.1.19",
            namespace: "foo-staging-db"
          }
        ]
      },
      {
        id: "bar",
        name: "Bar",
        cluster_id: "k8s.vendetta.io",
        releases: [
          { name: "postgres" }
        ]
      }
    ],
    repositories: [
      {
        name: "coreos",
        url: "https://s3-eu-west-1.amazonaws.com/coreos-charts/stable/"
      }
    ]
  },
  mutations: {

  },
  actions: {

  }
})
