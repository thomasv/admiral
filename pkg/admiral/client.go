package admiral

import (
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"k8s.io/helm/pkg/helm"
	"k8s.io/helm/pkg/helm/portforwarder"
	"k8s.io/helm/pkg/kube"
	rls "k8s.io/helm/pkg/proto/hapi/services"

	"fmt"
	"log"
)

type Client struct {
	Config *Config

	kubeClient kubernetes.Interface
	kubeConfig *rest.Config

	helmClient helm.Interface

	TillerNamespace      string
	TillerHost           string
	TillerConnectTimeout int64
	tunnel               *kube.Tunnel
}

const (
	DefaultTillerConnectTimeout int64 = 30
)

func NewClient(config *Config) (client *Client, err error) {
	client = &Client{
		Config: config,
	}

	return
}

// Creates an Admiral client from a local kubeConfig
func NewClientFromKubeconfig(kubeConfigPath, tillerNamespace string) (client *Client, err error) {
	kubeconfig, kubeclient, err := getKubeClient("", kubeConfigPath)

	client = &Client{
		kubeClient:           kubeclient,
		kubeConfig:           kubeconfig,
		TillerNamespace:      tillerNamespace,
		TillerConnectTimeout: DefaultTillerConnectTimeout,
	}

	log.Println("Created new admiral client from Kubeconfig", kubeConfigPath)

	return
}

// configForContext creates a Kubernetes REST client configuration for a given kubeConfig context.
func configForContext(context string, kubeConfig string) (*rest.Config, error) {
	config, err := kube.GetConfig(context, kubeConfig).ClientConfig()
	if err != nil {
		return nil, fmt.Errorf("could not get Kubernetes config for context %q: %s", context, err)
	}
	return config, nil
}

// getKubeClient creates a Kubernetes config and client for a given kubeConfig context.
func getKubeClient(context string, kubeConfig string) (*rest.Config, kubernetes.Interface, error) {
	config, err := configForContext(context, kubeConfig)
	if err != nil {
		return nil, nil, err
	}
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, nil, fmt.Errorf("could not get Kubernetes client: %s", err)
	}
	return config, client, nil
}

func (c *Client) Open() (err error) {
	tunnel, err := portforwarder.New(c.TillerNamespace, c.kubeClient, c.kubeConfig)
	if err != nil {
		log.Println("Error opening connection")
		return
	}

	c.tunnel = tunnel
	c.TillerHost = fmt.Sprintf("127.0.0.1:%d", tunnel.Local)

	options := []helm.Option{
		helm.Host(c.TillerHost),
		helm.ConnectTimeout(c.TillerConnectTimeout),
	}

	c.helmClient = helm.NewClient(options...)

	log.Printf("Opened connection to %s", c.TillerHost)

	return
}

func (c *Client) ListReleases() (res *rls.ListReleasesResponse, err error) {
	log.Println("Listing releases")

	return c.helmClient.ListReleases()
}

func (c *Client) Close() (err error) {
	log.Println("Closed connection")

	if c.tunnel != nil {
		c.tunnel.Close()
	}

	return
}
