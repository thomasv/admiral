package admiral

import (
	"testing"
)

func TestSetCluster(t *testing.T) {
	cluster := Cluster{Id: "foo-cluster", Name: "foo"}

	tiller := Tiller{Id: "foo-tiller"}
	tiller.SetCluster(&cluster)

	if tiller.cluster == nil {
		t.Fatal("Expected tiller.cluster to be set")
	}
}
