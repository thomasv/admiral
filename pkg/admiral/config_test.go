package admiral

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

var (
	configBytes = []byte(`{
		"name": "simple-config",
		"clusters": [
			{
				"id": "localhost",
				"name": "Local Docker for Mac Kubernetes Cluster",
				"kubeconfig_data": "YXBpVmVyc2lvbjogdjEKY2x1c3RlcnM6Ci0gY2x1c3RlcjoKICAgIHNlcnZlcjogaHR0cHM6Ly8xMjcuMC4wLjEKICBuYW1lOiBlMmUKY29udGV4dHM6Ci0gY29udGV4dDoKICAgIGNsdXN0ZXI6IGUyZQogICAgdXNlcjogdGVzdAogIG5hbWU6IGUyZQpjdXJyZW50LWNvbnRleHQ6ICIiCmtpbmQ6IENvbmZpZwpwcmVmZXJlbmNlczoge30KdXNlcnM6IFtdCg=="
			}
		],
		"tillers": [
			{
				"id": "foo",
				"cluster_id": "localhost",
				"namespace": "foo-tiller"
			}
		]
	}`)
)

func TestLoadConfigFromFile(t *testing.T) {
	tmpfile, err := ioutil.TempFile("", "simple-config-file-test")
	if err != nil {
		t.Fatal(err)
	}

	defer os.Remove(tmpfile.Name())

	if _, err := tmpfile.Write(configBytes); err != nil {
		t.Fatal(err)
	}

	if err := tmpfile.Close(); err != nil {
		t.Fatal(err)
	}

	config, err := LoadConfigFromFile(tmpfile.Name())

	assertSimpleConfigIsOk(t, config, err)
}

func TestLoadConfigFromBytes(t *testing.T) {
	config, err := LoadConfigFromBytes(configBytes)
	assertSimpleConfigIsOk(t, config, err)
}

func configForTesting() (config Config, err error) {
	return LoadConfigFromBytes(configBytes)
}

func assertSimpleConfigIsOk(t *testing.T, config Config, err error) {
	if err != nil {
		t.Fatalf("Err was not nil: %s", err.Error())
	}

	if config.Name != "simple-config" {
		t.Fatalf("Expected %s to be simple-config", config.Name)
	}

	if len(config.Clusters) != 1 {
		t.Fatalf("Expected %d to be 1", len(config.Tillers))
	}

	if len(config.Tillers) != 1 {
		t.Fatalf("Expected %d to be 1", len(config.Tillers))
	}

	fmt.Println("tiller", config.Tillers[0])

	if config.Tillers[0].cluster == nil {
		t.Fatalf("Cluster not set on Tiller")
	}

	if config.Tillers[0].Id != "foo" {
		t.Fatalf("Expected %s to be `foo`", config.Tillers[0].Id)
	}

	if config.Clusters[0].Id != "localhost" {
		t.Fatalf("Expected %s to be `localhost`", config.Clusters[0].Id)
	}
}
