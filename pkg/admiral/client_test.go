package admiral

import (
	"testing"
)

func TestClientInitializes(t *testing.T) {
	config, err := configForTesting()

	if err != nil {
		t.Fatalf("error for test config was not nil: %s", err.Error())
	}

	_, err = NewClient(&config)

	if err != nil {
		t.Fatalf("error for test client was not nil: %s", err.Error())
	}
}
