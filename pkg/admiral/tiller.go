package admiral

import (
	"fmt"
)

type Tiller struct {
	Id        string `json:"id"`
	Namespace string `json:"namespace"`
	ClusterId string `json:"cluster_id"`

	cluster *Cluster
}

func (t *Tiller) SetCluster(c *Cluster) {
	t.cluster = c
	fmt.Println("Set t", t)
}
