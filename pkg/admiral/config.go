package admiral

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Config struct {
	Name     string    `json:"name"`
	Tillers  []Tiller  `json:"tillers"`
	Clusters []Cluster `json:"clusters"`

	clustersById map[string]*Cluster
}

func LoadConfigFromBytes(configBytes []byte) (cfg Config, err error) {
	err = json.Unmarshal(configBytes, &cfg)

	if err != nil {
		return
	}

	err = cfg.Validate()

	return
}

func LoadConfigFromFile(configPath string) (cfg Config, err error) {
	dat, err := ioutil.ReadFile(configPath)

	if err != nil {
		return
	}

	cfg, err = LoadConfigFromBytes(dat)

	return
}

func (c *Config) Validate() (err error) {
	c.clustersById = make(map[string]*Cluster, len(c.Clusters))

	for _, cluster := range c.Clusters {
		c.clustersById[cluster.Id] = &cluster
	}

	for i, tiller := range c.Tillers {
		tillerCluster := c.clustersById[tiller.ClusterId]

		if tillerCluster == nil {
			return fmt.Errorf("cluster not found: %s", tiller.ClusterId)
		} else {
			c.Tillers[i].SetCluster(tillerCluster)
		}
	}

	return
}
