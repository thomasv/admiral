package admiral

type Cluster struct {
	Id               string `json:"id"`
	Name             string `json:"name"`
	KubeconfigBase64 string `json:"kubeconfig_data"`
}
