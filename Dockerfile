FROM golang as build
WORKDIR /go/src/github.com/thomasv314/admiral
ADD . /go/src/github.com/thomasv314/admiral
RUN go get -d ./...
RUN GOOS=linux GOARCH=amd64 go build -o admiral-bin .
RUN chmod u+x admiral-bin

FROM golang
COPY --from=build /go/src/github.com/thomasv314/admiral/admiral-bin /admiral
ENV HOME /tmp
USER nobody
ENTRYPOINT ["/admiral"]
