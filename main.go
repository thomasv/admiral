package main

import (
	"github.com/thomasv314/admiral/pkg/admiral"
	"log"
	"os"
)

var (
	client *admiral.Client
)

const (
	ConfigEnvVar      string = "ADMIRAL_CONFIG"
	DefaultConfigName string = "Admiralconfig"
)

func main() {
	var configName string

	if os.Getenv(ConfigEnvVar) == "" {
		configName = DefaultConfigName
	} else {
		configName = os.Getenv(ConfigEnvVar)
	}

	config, err := admiral.LoadConfigFromFile(configName)

	if err != nil {
		log.Fatalf("error loading config: %s", err.Error())
	}

	client, err := admiral.NewClient(&config)

	if err != nil {
		log.Fatalf("error creating client: %s", err.Error())
	}

	log.Println("Client!", client)
}
